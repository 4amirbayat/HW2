#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>



void get_text(std::ifstream&, std::string&);
std::vector<std::string> get_words(std::string&);
void show_words(std::vector<std::string>&);
std::vector<std::string> find_wrong_words(std::vector<std::string>&);
void show_bad_words(std::vector<std::string>&);
bool chk_vec(std::vector<std::string>&, std::string);	//checking if string exist in vector or not. for adding new strings
bool is_lower(std::string&);
